const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required!"]
	},
	email: {
		type: String,
		required: [true, "Email address is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	phoneNumber: {
		type: String,
		required: [true, "Please provide your cellphone number!"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	}, 
	dateAccountCreated: {
		type: Date,
		default: new Date
	}, 

	order: [{
			plantId: {
				type: String,
				required: [true, "Plant ID is required!"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			},
			quantity: {
				type: Number,
				default: 1
			},
			totalAmount: {
				type: Number
			}
	
			}],

			totalOrderAmount: {
				type: Number,
				default: 0
			}

})

module.exports = mongoose.model("User", userSchema);
