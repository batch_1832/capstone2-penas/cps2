const mongoose = require("mongoose");


const plantSchema = new mongoose.Schema({
	kind: {
		type: String,
		required: [true, "Choose the kind of plants that you like!"]
	},
	variety: {
		type: String,
		required: [true, "Choose the variety of plants that you wish to buy!"]
	},
	description: {
		type: String,
		required: [true, "Description is required!"]
	},
	
	price: {
		type: Number,
		required: [true, "Price is required!"]
	},
	stocks: {
		type: Number,
		required: [true, "Stocks is Required"]
	},
	isAvailable: {
		type: Boolean,
		default: true
	},

	ordered: [{
		userId: {
			type: String,
			required: [true, "User ID is required"]
		},
		orderedOn: {
			type: Date,
			default: new Date()
		},
		quantity: {
			type: Number,
			default: 1
		}
	}]

})

module.exports = mongoose.model("Plant", plantSchema);