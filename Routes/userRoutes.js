const express = require("express");
const router = express.Router();
const userControllers = require("../Controllers/userControllers");
const auth = require("../auth");



// Router for checking if the email exists
router.post("/checkEmail", (req, res) =>{
    userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));

})
// Registration
router.post("/register", (req, res) =>{
	userControllers.registerUser(req.body).then(result => res.send(result))

})

// login
router.post("/login", (req, res) =>{
	userControllers.logIn(req.body).then(result => res.send(result));
})

// Route for the retrieving the current user's details
router.get("/details", auth.verify, (req, res) =>{
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);

    // Provides the user's ID for the getProfile controller method
    userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})



// Set as admin
router.patch("/:customerId/setAsAdmin", auth.verify, (req, res) => {
    const customerData = auth.decode(req.headers.authorization);

    if(customerData.isAdmin){
        userControllers.setAsAdmin(req.params.customerId, req.body).then(result => res.send(result));
    }
    else{
        res.send("You can't access this page!");
    }
});


// Route to enroll a course
router.post("/addToCart", auth.verify, (req, res) =>{
    const userData = auth.decode(req.headers.authorization)

    let data = {
        // UserId will be retrirvrd from the request header
        userId: userData.id,
        plantId: req.body.plantId,
        quantity: req.body.quantity
    }
    if(userData.isAdmin){
        res.send("You're not allowed to access this page")
    }
    else{
        userControllers.addToCart(data).then(result => res.send(result));
    }

    
})

// GET orders

router.get("/userOrders", auth.verify, (req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    userControllers.orders(userData.id).then(result=> res.send(result))
})


// GET All Orders

router.get("/getAllOrders", auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){
        userControllers.getOrders().then(resultFromController=> res.send(resultFromController))  
    }
    else{
        res.send("You're not allowed to access this page")
    }
})

// mycart
router.get("/myCart", auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    if(userData !== isAdmin){
        userControllers.getCart().then(resultFromController=> res.send(resultFromController))  
    }
    else{
        res.send("You're not allowed to access this page")
    }
})





module.exports = router;