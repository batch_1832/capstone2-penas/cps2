const express = require("express");
const router = express.Router();
const plantControllers = require("../Controllers/plantControllers");
const auth = require("../auth");
const plantRoutes = require("../Routes/plantRoutes");




// add products
router.post("/", auth.verify, (req, res) =>{

const userData = auth.decode(req.headers.authorization);
if(userData.isAdmin){
	plantControllers.addPlants(req.body).then(result => res.send(result));
}
else{
	res.send ("false")
}

})


// Get products
router.get("/products", auth.verify, (req, res) =>{
	plantControllers.getAllPlants(req.body).then(result => res.send(result));

})

// Get all active

router.get("/activeProducts", (req, res) =>{
	plantControllers.getAllAvailable().then(result => res.send(result));
})


// Get with product Id

router.get("/:plantId", (req, res) =>{
	console.log(req.params.plantId);
	plantControllers.getById(req.params.plantId).then(result => res.send(result));
	
})

// Update Product
router.put("/:plantId", auth.verify, (req, res) =>{
									
	plantControllers.updateProduct(req.params.plantId, req.body).then(result => res.send(result));

})


// Archive a product

router.patch("/products/:plantId/archive", auth.verify, (req, res) =>{
	plantControllers.archiveProduct(req.params.plantId, req.body).then(result => res.send(result));
})


module.exports = router;