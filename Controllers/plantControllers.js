const Plant = require("../Models/Plant");

// add products
module.exports.addPlants = (reqBody) =>{
	let newPlant = new Plant({
		kind: reqBody.kind,
		variety: reqBody.variety,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks

	})

	return newPlant.save().then((plant, error) =>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}


// Other solution
// module.exports.addCourse = (reqBody, isAdmin) => {

// 	if (isAdmin) {

// 		let newCourse = new Course({
// 			name : reqBody.name,
// 			description : reqBody.description,
// 			price : reqBody.price,
// 			slots: reqBody.slots
// 		});

// 		return newCourse.save().then((course, error) => {
// 			if(error){
// 			return false
// 			}
// 			else{
// 				return true
// 			}
// 		});

// 	} else {
//		// Due to some updates, the return of the else statement cannot be read as a resolved promise in the .then method in the routes file so we need to use the "Promise.resolve" to be accepted in the routes function invoke.
//		// return "You don't have access to this page!"
// 		return Promise.resolve("You don't have access to this page!")
// 	};



// get all products

module.exports.getAllPlants = () => {
	return Plant.find({}).then(result => result);
}

// Get active products

module.exports.getAllAvailable = () =>{
	return Plant.find({isAvailable:true}).then(result=> result);

}

// GET with product Id
module.exports.getById = (plantId) =>{
	return Plant.findById(plantId).then(result => result);
}

// Update Product
module.exports.updateProduct = (plantId, reqBody) =>{
	let updatedProduct ={
		kind: reqBody.kind,
		variety: reqBody.variety,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}

	return Plant.findByIdAndUpdate(plantId, updatedProduct).then((plantUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}

// archive a product

module.exports.archiveProduct = (plantId, reqBody) =>{
	let updateActiveField = {
		isAvailable : reqBody.isAvailable
	}

	return Plant.findByIdAndUpdate(plantId, updateActiveField).then((isAvailable, error) =>{
		// Product is not archived
		if(error){
			return false;
		}
		// Product archived successfully
		else{
			return true
		}
	})
}