const User = require("../Models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Plant = require("../Models/Plant");


// Check if the email already exists
/*
	Steps:
	1. Use mongoose "find" method to find the duplicate emails.
	2. Use the ".then" method to send back a response to the front end application based on the result of find method.

*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// result is equal to an empty array ([]);
		// if result is greater than 0, user is found/already exists.
		if(result.length > 0){
			return true;
		}
		// No duplicate email found
		else{
			return false;
		}
	});
}


// Register
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		phoneNumber: reqBody.phoneNumber
	

	})
	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		else{
			console.log(user);
			return true;
		}
	})

	}


// logIn
module.exports.logIn = (reqBody) =>{
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return "Please create an account!";
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
	})
}

module.exports.getProfile = (data) =>{
	console.log(data)
	return User.findById(data.userId).then(result =>{
		result.password ="";

		return result;
	})
}

//Set as admin
module.exports.setAsAdmin = (userId, reqBody) => {
    let updatedStatus = {
        isAdmin: reqBody.isAdmin
    }

    return User.findByIdAndUpdate(userId, updatedStatus).then((success, err) => {
        if(err){
            return false;
        }
        else{
            return "Successfully Updated Status!"
        }
    })
};


// Create an order

module.exports.addToCart = async (data) =>{

	let productPrice = await Plant.findById(data.plantId).then(product=>product.price)
	let productStock = await Plant.findById(data.plantId).then(product=>product.stocks)
	let productStatus = await Plant.findById(data.plantId).then(product=>product.isAvailable)
															
	if(productStock >= 0 && productStatus){
		let isUserUpdated = await User.findById(data.userId).then(user =>{
		// add the courseId in the users enrollment array
		user.order.push({plantId: data.plantId,
						quantity: data.quantity,
						totalAmount: productPrice * data.quantity
					})

		// saves the updated user information in the database.
		return user.save().then((ordered, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	
	

	let isOrderUpdated = await Plant.findById(data.plantId).then(plant =>{
		plant.stocks -= data.quantity
		plant.ordered.push({userId: data.userId,
							user: data.userId,
							quantitySold: data.quantity
						})

		
		// saves the updated course information in the database
		return plant.save().then((orders, error) =>{
			if(error){
				return false;

			}
			else{
				return true;
			}
		})
	})

	

	if(isUserUpdated && isOrderUpdated){
		return true;
	}
	// User enrollment failure
	else{
		return false;
	}
}
	else{
		return "Product Unavailable"
	}
}

// GET Orders

module.exports.orders = (userId) =>{
	return User.findById(userId).then(result=>{
		return result.orders
	})
}


// Get all orders
module.exports.getOrders = () =>{
	return User.find({},{
		password:0,
		_Id:0,
		createdOn:0,
		__v:0

	}).then(result=>result)
}

//My Cart

module.exports.getCart = (request, response) => {

    const userId = auth.decode(request.headers.authorization).id;

    return User.findById(userId).then((user, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            });
        }

        return response.send(user.userCart)
    })

}