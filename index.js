const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./Routes/userRoutes");
const plantRoutes = require("./Routes/plantRoutes");




const app = express();
const port = 4000;

// MongoDB
mongoose.connect("mongodb+srv://admin:admin@myfirstcluster.bcygx.mongodb.net/CPS2_Ecmrce?retryWrites=true&w=majority", {useNewURLParser: true, useUnifiedTopology: true});


let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Middlewares

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// API Routes
app.use("/customers", userRoutes);
app.use("/plants", plantRoutes);


app.listen(process.env.PORT || port, () =>{
	console.log(`API is now active in port ${process.env.PORT || port}`);
})